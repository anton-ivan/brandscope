require 'rails_helper'

describe Analysis::CampaignSummaryReportController, type: :controller do
  let(:user) { sign_in_as_user }
  let(:company_user) { user.current_company_user }
  let(:company) { user.companies.first }
  let(:campaign1) { create(:campaign, company: company, name: 'Test Campaign FY01') }
  let(:campaign2) { create(:campaign, company: company, name: 'CFY12') }
  let(:event1) { create(:submitted_event, campaign: campaign1, company: company) }
  let(:event2) { create(:late_event, campaign: campaign2, company: company) }

  before { user }

  describe "GET 'index'" do
    it 'should return http success' do
      get 'index'
      expect(response).to be_success
    end
  end

  describe "GET 'items'" do
    let(:user1) { create(:company_user, user: create(:user, first_name: 'Roberto', last_name: 'Gomez'), company: company) }
    let(:user2) { create(:company_user, user: create(:user, first_name: 'Mario', last_name: 'Moreno'), company: company) }

    it 'should return correct count of events by Team in People bucket' do
      company_user.campaigns << campaign1
      company_user.campaigns << campaign2

      membership1 = create(:membership, company_user: user.company_users.first, memberable: event1)
      company_user.memberships << membership1
      membership2 = create(:membership, company_user: user.company_users.first, memberable: event2)
      company_user.memberships << membership2

      team = create(:team, name: 'Team 1', company: company)
      team.memberships << membership1

      campaign_ids = "%d, %d" % [campaign1.id, campaign2.id]
      get :items, :campaign_summary => campaign_ids
      expect(response).to be_success
      expect(response.body).to have_selector("span.results-count", :text => "2")

      get :items, :campaign_summary => campaign_ids, :team => [team.id]
      expect(response).to be_success
      expect(response.body).to have_selector("span.results-count", :text => "1")
    end

    it 'should return correct count of events by custom saved filter' do
      event1.users << user1
      event2.users << user2
      Sunspot.commit

      filter = create(:custom_filter,
             owner: company_user, name: 'Custom Filter', apply_to: 'campaign_summary_report',
             filters: 'campaign%5B%5D=' + campaign1.to_param + '&user%5B%5D=' + user1.to_param +
                 '&event_status%5B%5D=Submitted&status%5B%5D=Active')

      # Using Custom Filter
      campaign_ids = "%d, %d" % [campaign1.id, campaign2.id]
      get :items, :campaign_summary => campaign_ids, :cfid => [filter.id]

      expect(response).to be_success
      expect(response.body).to have_selector("span.results-count", :text => "1")
      expect(response.body).to have_content(filter.name)
    end
  end
end
